package com.nlmk.korotkov.jse29;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Service {

    public long sum(String arg1, String arg2) {
        // преобразование типа
        Long longArg1 = getLong(arg1);
        Long longArg2 = getLong(arg2);
        return longArg1 + longArg2;
    }

    public long factorial(String arg) {
        Long longArg = getLong(arg);
        // проверяем, что число положительное
        checkPositive(longArg);
        Long result = 1L;
        while (longArg > 1) {
            try {
                // перемножаем элементы в обратном порядке - начиная с наибольшего значения
                result = Math.multiplyExact(result, longArg);
            } catch (ArithmeticException e) {
                throw new IllegalArgumentException();
            }
            longArg--;
        }
        return result;
    }

    public long[] fibonacci(String arg) {
        Long longArg = getLong(arg);
        checkPositive(longArg);
        // создаем массив
        List<Long> resultList = new ArrayList<>();
        // добавили в него 2 элемента
        resultList.add(0L);
        resultList.add(1L);
        int index = 1;

        // последовательно складываем элементы ряда Фибоначчи, пока не дойдем до заданного
        while (longArg > resultList.get(index - 1) + resultList.get(index)) {
            // находим следующий элемент ряда Фибоначчи
            resultList.add(resultList.get(index - 1) + resultList.get(index));
            index++;
        }
        // сработает, если было задано нецелое число
        if (!Objects.equals(longArg, resultList.get(index - 1) + resultList.get(index))) {
            throw new IllegalArgumentException();
        }

        // копируем в другой массив
        long[] result = new long[resultList.size()];
        for (index = 0; index < resultList.size(); index++) {
            result[index] = resultList.get(index);
        }
        return result;
    }

    private long getLong(String arg) {
        // преобразование типа и создание исключение в случае неудачи
        try {
            return Long.parseLong(arg);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException();
        }
    }

    private void checkPositive(long arg) {
        // проверка, что число положительное
        if (arg < 0) {
            throw new IllegalArgumentException();
        }
    }

}
