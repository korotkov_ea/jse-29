package com.nlmk.korotkov.jse29;

import java.util.Arrays;
import java.util.Objects;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Service service = new Service();
        Scanner scanner = new Scanner(System.in);


        String command = "help";
        while (!Objects.equals(command, "exit")) {
            switch (command) {
                case "sum": {
                    System.out.print("Input first argument: ");
                    String in1 = scanner.nextLine();
                    System.out.print("Input second argument: ");
                    String in2 = scanner.nextLine();
                    System.out.print("Result of computing: ");
                    System.out.println(service.sum(in1, in2));
                    break;
                }
                case "factorial": {
                    System.out.print("Input first argument: ");
                    String in = scanner.nextLine();
                    System.out.print("Result of computing: ");
                    System.out.println(service.factorial(in));
                    break;
                }
                case "fibonacci": {
                    System.out.print("Input first argument: ");
                    String in = scanner.nextLine();
                    System.out.print("Result of computing: ");
                    System.out.println(Arrays.toString(service.fibonacci(in)));
                    break;
                }
                case "help": {
                    System.out.println("List of commands: ");
                    System.out.println("    sum - computing sum");
                    System.out.println("    factorial - computing factorial");
                    System.out.println("    fibonacci - razlozenie na summu chisel Fibonacci");
                    System.out.println("    help - help");
                    System.out.println("    exit - exit");
                    break;
                }
                default: {
                    System.out.println("List of commands: ");
                    System.out.println("    sum - computing sum");
                    System.out.println("    factorial - computing factorial");
                    System.out.println("    fibonacci - razlozenie na summu chisel Fibonacci");
                    System.out.println("    help - help");
                    System.out.println("    exit - exit");
                }
            }
            System.out.print("Input command: ");
            command = scanner.nextLine();
        }
    }
}
